import time
import pyautogui
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
website = "https://skyarc-test.ru"
time.sleep(5)

driver.get(website)
time.sleep(10)

navBar = driver.find_element(By.XPATH, '//*[@id="navBarItemProfile"]/div/label')  # нажать на кнопку профиль.
navBar.click()
time.sleep(5)

linkRegistration = driver.find_element(By.XPATH, '//span[@class="fnt1 clb clactive pointer"]')  # ссылка регистрации.
linkRegistration.click()
time.sleep(5)

registrationButton = driver.find_element(By.XPATH, '//div[@id="buttonSubmitDialogSignUp"]')
registrationButton.click()
time.sleep(1)

checkMessage = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert checkMessage.text == 'Введите корректные данные'

email = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[4]/div[2]/span')
assert email.text == 'Не может быть пустым'
time.sleep(1)

name = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[5]/div[2]/span')
assert name.text == 'Не может быть пустым'
time.sleep(1)

surName = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[6]/div[2]/span')
assert surName.text == 'Не может быть пустым'
time.sleep(5)

mobile = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[8]/div[2]/span')
assert mobile.text == 'Не может быть пустым'

time.sleep(1)

password = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[10]/div[2]/span')
assert password.text == 'Не может быть пустым'
time.sleep(1)

repeatPassword = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div/div[11]/div[2]/span')
assert repeatPassword.text == 'Не может быть пустым'
time.sleep(5)

photo = driver.find_element(By.XPATH, '//div[@id="buttonOpenFileDialogSignUp"]')
time.sleep(5)
photo.click()
time.sleep(5)
pyautogui.write(r"c:\3.docx")
time.sleep(5)
pyautogui.press("enter")
time.sleep(1)

uploadPhoto = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert uploadPhoto.text == 'Неверное расширение файла'
time.sleep(5)

photo = driver.find_element(By.XPATH, '//div[@id="buttonOpenFileDialogSignUp"]')
time.sleep(5)
photo.click()
time.sleep(5)
pyautogui.write(r"C:\2.pdf")
pyautogui.press("enter")
time.sleep(1)

uploadPhoto = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert uploadPhoto.text == 'Размер файла более 3МБ: 9.19МБ'
time.sleep(5)

driver.quit()
