import time
import pyautogui
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
website = "https://skyarc-test.ru"
time.sleep(5)

driver.get(website)
time.sleep(10)

link = driver.find_element(By.XPATH, '//*[@id="navBarItemProfile"]/div/label')
link.click()
time.sleep(5)

loginUser = driver.find_element(By.XPATH, '//*[@id="dialogSigninEmailInput"]')
loginUser.send_keys("s.ibragimova@aeroscript.ru")

passUser = driver.find_element(By.XPATH, '//*[@id="dialogSigninPasswordInput"]')
passUser.send_keys("11111111")

loginButton = driver.find_element(By.XPATH, '//*[@id="buttonSubmitDialogSignIn"]')
loginButton.click()
time.sleep(5)

nawBarBVS = driver.find_element(By.XPATH, '//*[@id="navBarItemDrones"]')
nawBarBVS.click()

pressNewBVS = driver.find_element(By.XPATH, '//*[@id="buttonAddDroneDialogDrones"]')
pressNewBVS.click()

pressSave = driver.find_element(By.XPATH, '//*[@id="buttonSubmitDialogDroneEdit"]')
pressSave.click()
time.sleep(1)

checkMessage = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert checkMessage.text == 'Введите корректные данные'

name = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div[2]/div[2]/span')
assert name.text == 'Не может быть пустым'
time.sleep(1)

serialNumber = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div[2]/div[2]/span')
assert serialNumber.text == 'Не может быть пустым'
time.sleep(5)

model = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div[3]/div[2]/span')
assert model.text == 'Не может быть пустым'

time.sleep(5)

data = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div[4]/div/div/div[2]/span')
assert data.text == 'Не может быть пустым'
time.sleep(5)

massaVBS = driver.find_element(By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div/div[5]/div[2]/span')
assert massaVBS.text == 'Не может быть пустым'
time.sleep(5)

photo = driver.find_element(By.XPATH, '//div[@id="buttonOpenFileDialogDroneEdit"]')
photo.click()
time.sleep(5)
pyautogui.write(r"c:\3.docx")
time.sleep(10)
pyautogui.press("enter")
time.sleep(1)

uploadPhoto = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert uploadPhoto.text == 'Неверное расширение файла'
time.sleep(5)

massaVBS2 = driver.find_element(By.XPATH, '//*[@id="dialogDroneWeightInput"]')
massaVBS2.send_keys('500')

serialNumber2 = driver.find_element(By.XPATH, '//*[@id="dialogDroneRegNoInput"]')
serialNumber2.send_keys('123')
time.sleep(10)

uploadDocument = driver.find_element(By.CSS_SELECTOR, "#app > div > div.flxcol.dialog-wrapper > div.flxcol.flx1.dialog-wrapper-content-wrapper > div > div > div:nth-child(7) > div.flxrow.alitc.gap8.input-wrapper-controls > svg.icon.icon20.cle.clactive.pointer > path")
uploadDocument.click()
time.sleep(10)

nameDocument = driver.find_element(By.XPATH, '//*[@id="modalAddDocumentTitleInput"]')
nameDocument.send_keys('123')
time.sleep(10)

downloadPC = driver.find_element(By.XPATH, '//*[@id="openFileModalAddDocument"]')
downloadPC.click()
time.sleep(5)
pyautogui.write(r"c:\2.pdf")
time.sleep(10)
pyautogui.press("enter")
time.sleep(1)

uploadPhoto = driver.find_element(By.XPATH, '//div[@class="v-snackbar-wrapper"]')
assert uploadPhoto.text == 'Размер файла более 3МБ: 9.19МБ'
time.sleep(5)
driver.quit()


